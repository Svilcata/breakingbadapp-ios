//
//  ApiDefinition.swift
//  BreakingBadApp
//
//  Created by Adastra on 13.11.19.
//  Copyright © 2019 Svilen. All rights reserved.
//

import Alamofire

protocol ApiDefinition {
    func getCharacters(completionHandler: @escaping (DataResponse<[CharacterModel], AFError>) -> Void)
}
