//
//  Router.swift
//  BreakingBadApp
//
//  Created by Adastra on 13.11.19.
//  Copyright © 2019 Svilen. All rights reserved.
//

import Alamofire

enum Router : URLRequestConvertible {
    
    case getCharacters
    
    static let baseURLString = "https://breakingbadapi.com/"
    
    var path: String {
       return "api/characters"
    }
    
    var param: Parameters? {
        switch self {
            
        default:
            return nil
        }
    }
    
    var method: HTTPMethod {
        return .get
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try Router.baseURLString.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = self.method.rawValue

        return try URLEncoding.default.encode(urlRequest, with: self.param)
    }
}
