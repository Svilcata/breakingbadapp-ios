//
//  ApiClient.swift
//  BreakingBadApp
//
//  Created by Adastra on 13.11.19.
//  Copyright © 2019 Svilen. All rights reserved.
//
import Alamofire

final class ApiClient: ApiDefinition {
    func getCharacters(completionHandler: @escaping (DataResponse<[CharacterModel], AFError>) -> Void) {
        AF.request(Router.getCharacters).responseDecodable(completionHandler: completionHandler)
    }
}
