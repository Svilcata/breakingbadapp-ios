//
//  Character.swift
//  BreakingBadApp
//
//  Created by Adastra on 13.11.19.
//  Copyright © 2019 Svilen. All rights reserved.
//

class CharacterModel: Codable {
    let appearance: [Int]?
    let better_call_saul_appearance: [Int]?
    let birthday:String
    let category:String
    let char_id:Int
    let img:String
    let name:String
    let nickname:String
    let occupation: [String]?
    let portrayed:String
    let status:String
}
