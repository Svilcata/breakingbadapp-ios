//
//  CharacterCell.swift
//  BreakingBadApp
//
//  Created by Adastra on 13.11.19.
//  Copyright © 2019 Svilen. All rights reserved.
//

import UIKit

class CharacterCell: UITableViewCell {

    @IBOutlet weak var characterImg: UIImageView!
    @IBOutlet weak var characterNameLabel: UILabel!
    @IBOutlet weak var characterPortrayedByLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
