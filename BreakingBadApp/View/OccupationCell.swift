//
//  OccupationCell.swift
//  BreakingBadApp
//
//  Created by Adastra on 14.11.19.
//  Copyright © 2019 Svilen. All rights reserved.
//

import UIKit

class OccupationCell: UITableViewCell {

    @IBOutlet weak var occupationLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
