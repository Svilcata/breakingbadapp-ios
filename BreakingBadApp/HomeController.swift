//
//  ViewController.swift
//  BreakingBadApp
//
//  Created by Adastra on 13.11.19.
//  Copyright © 2019 Svilen. All rights reserved.
//

import UIKit
import AlamofireImage

final class HomeController: UIViewController {
    
    // MARK: - Outlets
    
    @IBOutlet private weak var searchTxtField: UITextField!
    @IBOutlet private weak var tableView: UITableView!
    
    private var spinner: UIView?
    private var picker: UIPickerView = UIPickerView()
    
    var charactersDownloaded = [CharacterModel]()
    var characters = [CharacterModel]()
    var character: CharacterModel?
    let seasonArray = ["All", "1", "2", "3", "4", "5"]
    
    @IBAction private func textChanged(_ sender: UITextField) {
        if sender.text?.isEmpty ?? false {
            self.characters = self.charactersDownloaded
            tableView.reloadData()
        }
    }
    
    @IBAction private func filterClicked(_ sender: UIButton) {
        showSeasonPicker(onView: self.view)
    }
    
    @IBAction private func searchClicked(_ sender: UIButton) {
        if !(searchTxtField.text?.isEmpty ?? true) {
            self.characters = charactersDownloaded.filter({ (it) -> Bool in
                it.name.lowercased().contains(searchTxtField.text!.lowercased())
            })
            tableView.reloadData()
        }
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
           
        self.showSpinner(onView: self.view)
           
        self.tableView.register(UINib(nibName: "CharacterCell", bundle: nil),
                                   forCellReuseIdentifier: "CharacterCell")
           
        ApiClient.init().getCharacters { [weak self] (result) in
            self?.characters = result.value ?? []
            self?.charactersDownloaded = self?.characters ?? []
            self?.tableView.reloadData()
            self?.removeSpinner()
        }
    }
    
    // MARK: - Navigation
       
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       let vc = segue.destination as! DetailController
       vc.selectedChar = character
    }
}
    
extension HomeController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
       removePicker()
       
       if row != 0 {
           self.characters = charactersDownloaded.filter({ (it) -> Bool in
               (it.appearance?.contains(row) ?? false)
           })
       } else {
           self.characters = self.charactersDownloaded
       }
       tableView.reloadData()
    }
       
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return seasonArray[row]
    }
}
    
extension HomeController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
       return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
       return seasonArray.count
    }
}
    
extension HomeController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return characters.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "CharacterCell", for: indexPath) as! CharacterCell
        let character = characters[indexPath.row]
        
        cell.characterImg.af_setImage(withURL: URL(string: character.img)!)
        cell.characterNameLabel.text = character.name
        cell.characterPortrayedByLabel.text = character.portrayed
        
        return cell
    }
}

extension HomeController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.character = characters[indexPath.row]
        self.performSegue(withIdentifier: "segueDetailScreen", sender: self)
    }
}
// MARK: - Spinner

private extension HomeController {
    func showSpinner(onView: UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        self.spinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            self.spinner?.removeFromSuperview()
            self.spinner = nil
        }
    }
}

// MARK: - Picker

private extension HomeController {
    func showSeasonPicker(onView: UIView) {
        picker.backgroundColor = UIColor.lightGray.withAlphaComponent(0.3)
        picker.delegate = self
        picker.dataSource = self
        self.view.addSubview(picker)
        picker.center = self.view.center
    }
    
    func removePicker() {
        DispatchQueue.main.async {
            self.picker.removeFromSuperview()
        }
    }
}
