//
//  DetailController.swift
//  BreakingBadApp
//
//  Created by Adastra on 13.11.19.
//  Copyright © 2019 Svilen. All rights reserved.
//

import UIKit
import AlamofireImage

class DetailController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    // MARK: - Outlets
    
    @IBOutlet weak var selectedCharacterImg: UIImageView!
    @IBOutlet weak var selectedCharacterNameLabel: UILabel!
    @IBOutlet weak var selectedCharacterNicknameLabel: UILabel!
    @IBOutlet weak var selectedCharacterStatusLabel: UILabel!
    @IBOutlet weak var occupationTableView: UITableView! {
        didSet {
            setupOccupationTableView()
        }
    }
    @IBOutlet weak var appearanceTableView: UITableView! {
        didSet {
            setupAppearanceTableView()
        }
    }
    
    var selectedChar: CharacterModel!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        populate()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == appearanceTableView {
            return tableView.dequeueReusableHeaderFooterView(withIdentifier: "AppearanceHeaderCell") as! AppearanceHeaderCell
        } else if tableView == occupationTableView {
            return tableView.dequeueReusableHeaderFooterView(withIdentifier: "OccupationHeaderCell") as! OccupationHeaderCell
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == appearanceTableView {
            return selectedChar?.appearance?.count ?? 0
        } else if tableView == occupationTableView {
            return selectedChar?.occupation?.count ?? 0
        }
        return 0
    }
       
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == appearanceTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AppearanceCell", for: indexPath) as! AppearanceCell
            guard let appearance = selectedChar?.appearance?[indexPath.row] else {
                return UITableViewCell()
            }
            cell.appearanceLabel.text = "\(appearance)"
            return cell
        } else if tableView == occupationTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "OccupationCell", for: indexPath) as! OccupationCell
            cell.occupationLabel.text = selectedChar?.occupation?[indexPath.row]
            return cell
        }
        
        // !!! Unknown table view
        fatalError()
    }
}

// MARK: - Setup UI

private extension DetailController {
    func setupOccupationTableView() {
        self.occupationTableView.register(UINib(nibName: "OccupationHeaderCell", bundle: nil),
                                          forHeaderFooterViewReuseIdentifier: "OccupationHeaderCell")
        self.occupationTableView.register(UINib(nibName: "OccupationCell", bundle: nil), forCellReuseIdentifier: "OccupationCell")
    }

    func setupAppearanceTableView() {
        self.appearanceTableView.register(UINib(nibName: "AppearanceHeaderCell", bundle: nil),
                                          forHeaderFooterViewReuseIdentifier: "AppearanceHeaderCell")
        self.appearanceTableView.register(UINib(nibName: "AppearanceCell", bundle: nil), forCellReuseIdentifier: "AppearanceCell")
    }
}

// MARK: - Populate

private extension DetailController {
    func populate() {
        selectedCharacterImg.af_setImage(withURL: URL(string: selectedChar.img)!)
        selectedCharacterNameLabel.text = selectedChar.name
        selectedCharacterNicknameLabel.text = selectedChar.nickname
        selectedCharacterStatusLabel.text = selectedChar.status
    }
}
